#ifndef GAMEAPP_H
#define GAMEAPP_H
#include "d3dApp.h"
#include "lgihtClass.h"
#include "Camera.h"
#include "MeshGeometryClass.h"
#define MAX_INSTANCE_CONSTANTS 100
#define IMAGE_HEIGHT 1024
#define IMAGE_WIDTH 1024
class GameApp : public D3DApp
{
public:
    GameApp(HINSTANCE hInstance);
    ~GameApp();

    bool Init();
    void OnResize();
    void UpdateScene(float dt);
    void DrawScene();


    //struct VSConstantBuffer
    //{
    //    DirectX::XMMATRIX world;
    //    DirectX::XMMATRIX view;
    //    DirectX::XMMATRIX proj;
    //    DirectX::XMMATRIX worldInvTranspose;
    //};
    //struct PSConstantBuffer
    //{
    //    DirLit dirlit;
    //};

    struct CBufferReFreashFrequently
    {
        DirectX::XMMATRIX world;
        DirectX::XMMATRIX view;
        DirectX::XMMATRIX worldInvTranspose;
        DirectX::XMMATRIX boneTransform[100];
    };
    struct CBufferReFreashRarely
    {
        DirLit dirlit;
        DirectX::XMINT4 g_InstanceMatricesWidth;
    };
    struct CBufferReFreshOnResize
    {
         DirectX::XMMATRIX proj;
    };
    struct InstanceDataElelment
    {
        DirectX::XMFLOAT4 world1;
        DirectX::XMFLOAT4 world2;
        DirectX::XMFLOAT4 world3;
        DirectX::XMFLOAT4 color;

        DirectX::XMUINT2 indexAndOffset;
        DirectX::XMUINT2 animationIndexAndEndOffset;
    };
    struct CBufferInstance
    {
        InstanceDataElelment instancePos[MAX_INSTANCE_CONSTANTS];
    };

    ComPtr<ID3D11ShaderResourceView> m_pWood;
    ComPtr<ID3D11ShaderResourceView> m_pAnimaTexture;
    ComPtr<ID3D11SamplerState> m_pSamplerState;
    ComPtr<ID3D11RasterizerState> m_pRsaterizarState;

private:
    bool InitEffect();
    bool InitResource();

private:
    ComPtr<ID3D11InputLayout> m_pVertexLayout;	// 顶点输入布局
    ComPtr<ID3D11Buffer> m_pVertexBuffer;		// 顶点缓冲区
    ComPtr<ID3D11VertexShader> m_pVertexShader;	// 顶点着色器
    ComPtr<ID3D11PixelShader> m_pPixelShader;	// 像素着色器
    ComPtr<ID3D11Buffer> m_pindexBuffer;

    ComPtr<ID3D11VertexShader> m_pVertexShader2;	// 顶点着色器
    ComPtr<ID3D11PixelShader> m_pPixelShader2;	// 像素着色器

    ComPtr <ID3D11Buffer> m_pConstantBuffer[4];

    std::shared_ptr<Camera> m_pCamera;
    CameraMode m_cameraMode;
    
    CBufferReFreashFrequently m_cBufferFrequently;
    CBufferReFreashRarely m_cBufferRarely;
    CBufferReFreshOnResize m_cBufferResize;
    CBufferInstance m_cBufferInstance;
    std::vector<InstanceDataElelment> m_instanceData;

    DirLit m_dirLit;
    
    MeshGeometryClass* g1;
    MeshGeometryClass *plane;
    GameTimer timer;

    bool isFinshed;
    bool needBake;
    bool isStart;
    int animaFramesCount;
    int currentFrames;
    int FPS;
    int starOffset;
    int perRowBoneAmount;
    float *m_pixel;
    float timeCount;
    float maxValue;
    void BakeBoneImage();
};


#endif
