
#ifndef CAMERA_H
#define CAMERA_H

#include <d3d11_1.h>
#include <DirectXMath.h>
#include "Transform.h"


class Camera
{
public:
    Camera() = default;
    virtual ~Camera() = 0;


    DirectX::XMVECTOR GetPositionXM() const;
    DirectX::XMFLOAT3 GetPosition() const;


    float GetRotationX() const;

    float GetRotationY() const;


    DirectX::XMVECTOR GetRightAxisXM() const;
    DirectX::XMFLOAT3 GetRightAxis() const;
    DirectX::XMVECTOR GetUpAxisXM() const;
    DirectX::XMFLOAT3 GetUpAxis() const;
    DirectX::XMVECTOR GetLookAxisXM() const;
    DirectX::XMFLOAT3 GetLookAxis() const;


    DirectX::XMMATRIX GetViewXM() const;
    DirectX::XMMATRIX GetProjXM() const;
    DirectX::XMMATRIX GetViewProjXM() const;


    D3D11_VIEWPORT GetViewPort() const;



    void SetFrustum(float fovY, float aspect, float nearZ, float farZ);


    void SetViewPort(const D3D11_VIEWPORT& viewPort);
    void SetViewPort(float topLeftX, float topLeftY, float width, float height, float minDepth = 0.0f, float maxDepth = 1.0f);

protected:

    // 摄像机的变换
    Transform m_Transform = {};

    // 视锥体属性
    float m_NearZ = 0.0f;
    float m_FarZ = 0.0f;
    float m_Aspect = 0.0f;
    float m_FovY = 0.0f;

    // 当前视口
    D3D11_VIEWPORT m_ViewPort = {};

};

class FirstPersonCamera : public Camera
{
public:
    FirstPersonCamera() = default;
    ~FirstPersonCamera() override;


    void SetPosition(float x, float y, float z);
    void SetPosition(const DirectX::XMFLOAT3& pos);

    void LookAt(const DirectX::XMFLOAT3& pos, const DirectX::XMFLOAT3& target, const DirectX::XMFLOAT3& up);
    void LookTo(const DirectX::XMFLOAT3& pos, const DirectX::XMFLOAT3& to, const DirectX::XMFLOAT3& up);

    void Strafe(float d);    // 平移

    void Walk(float d);    // 直行(平面移动)

    void MoveForward(float d);    // 前进(朝前向移动)

    void Pitch(float rad);

    void RotateY(float rad);
};

class ThirdPersonCamera : public Camera
{
public:
    ThirdPersonCamera() = default;
    ~ThirdPersonCamera() override;


    DirectX::XMFLOAT3 GetTargetPosition() const;    // 获取当前跟踪物体的位置

    float GetDistance() const;    // 获取与物体的距离

    void RotateX(float rad);    // 绕物体垂直旋转(注意绕x轴旋转欧拉角弧度限制在[0, pi/3])

    void RotateY(float rad);    // 绕物体水平旋转

    void Approach(float dist);    // 拉近物体

    void SetRotationX(float rad);    // 设置初始绕X轴的弧度(注意绕x轴旋转欧拉角弧度限制在[0, pi/3])

    void SetRotationY(float rad);    // 设置初始绕Y轴的弧度

    void SetTarget(const DirectX::XMFLOAT3& target);    // 设置并绑定待跟踪物体的位置

    void SetDistance(float dist);    // 设置初始距离

    void SetDistanceMinMax(float minDist, float maxDist);    // 设置最小最大允许距离

private:
    DirectX::XMFLOAT3 m_Target = {};
    float m_Distance = 0.0f;

    float m_MinDist = 0.0f, m_MaxDist = 0.0f;    // 最小允许距离，最大允许距离
};


#endif

