#include "Triangle.hlsli"

VertexPosHWNormalTex VShader(VertexIn v)
{
    VertexPosHWNormalTex o;
    o.pos = mul(float4(v.pos,1.0f),world);
    o.pos = mul(o.pos, view);
    o.pos = mul(o.pos, proj);
    o.posW = mul(float4(v.pos, 1.0f), world);

   // o.normalW = mul(v.normal, (float3x3)worldInvTranspose);

    o.normalW = v.normal;
    o.uv = v.uv;
    return o;
}