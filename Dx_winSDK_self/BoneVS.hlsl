#include "Triangle.hlsli"

Texture2D texAnimation :  register(t1);
SamplerState g_samPoint
{
    Filter = MIN_MAG_MIP_POINT;
    AddressU = Wrap;
    AddressV = Wrap;
};

float4x4 loadBoneMatrix(uint4 animationData, uint bone)
{
    float4x4 rval = g_Identity;

    uint baseIndex = (animationData.x + animationData.y) * 1024;
    baseIndex += (4 * bone);

    uint baseU = baseIndex % 1024;
    uint baseV = baseIndex / 1024;

    float4 mat1 = gTex.Load(uint3(baseU, baseV, 0)) * 2 - 1;;
    float4 mat2 = gTex.Load(uint3(baseU + 1, baseV, 0)) * 2 - 1;
    float4 mat3 = gTex.Load(uint3(baseU + 2, baseV, 0)) * 2 - 1;

    rval = float4x4(mat1.x, mat2.x, mat3.x, 0.0f,
        mat1.y, mat2.y, mat3.y, 0.0f,
        mat1.z, mat2.z, mat3.z, 0.0f,
        mat1.w, mat2.w, mat3.w, 1.0f);

    return rval;
}

VertexPosHWNormalTex VShader(SkinnedVertexIn v)
{
    uint4 animationData = g_instance[v.instanceId].animationData;
    float4 worldMatrix1 = g_instance[v.instanceId].world1;
    float4 worldMatrix2 = g_instance[v.instanceId].world2;
    float4 worldMatrix3 = g_instance[v.instanceId].world3;
    //float4 instancColor = g_instance[v.instanceId].color;

    animationData.x = 0;
    animationData.y = 0;


    matrix Trasnform = loadBoneMatrix(animationData, v.boneIndiecs.x);
    if (v.weights.y > 0)
    {
        Trasnform += loadBoneMatrix(animationData, v.boneIndiecs.y) * v.weights.y;
        if (v.weights.z > 0)
        {
            Trasnform += loadBoneMatrix(animationData, v.boneIndiecs.z) * v.weights.z;
        }
        if (v.weights.w > 0)
        {
            Trasnform += loadBoneMatrix(animationData, v.boneIndiecs.w) * v.weights.w;
        }
    }

    //matrix Trasnform = boneTransform[v.boneIndiecs[0]] * v.weights.x;
    //Trasnform += boneTransform[v.boneIndiecs[1]] * v.weights.y;
    //Trasnform += boneTransform[v.boneIndiecs[2]] * v.weights.z;
    //Trasnform += boneTransform[v.boneIndiecs[3]] * v.weights.w;

    VertexPosHWNormalTex o;
    o.pos = mul(float4(v.pos, 1.0f), Trasnform);
   // o.pos = mul(float4(v.pos, 1.0f), world);

    //float4x4 instanceTransform = float4x4(g_instance[v.instanceId].world1,
    //    g_instance[v.instanceId].world2,
    //    g_instance[v.instanceId].world3,
    //    0.0f, 0.0f, 0.0f, 1.0f);

    float4x4 instanceTransform = float4x4(1.0f, 0.0f, 0.0f, 0.0f,
        0.0f, 1.0f, 0.0f, 0.0f,
        0.0f, 0.0f, 1.0f, 0.0f, 
        0.0f, 0.0f, 0.0f, 1.0f);

    o.pos = mul(o.pos, instanceTransform);
    o.pos = mul(o.pos , world);
    o.pos = mul(o.pos, view);
    o.pos = mul(o.pos, proj);

    //// o.normalW = mul(v.normal, (float3x3)worldInvTranspose);

    float3 normalL = float3(0.0f, 0.0f, 0.0f);
    normalL += v.weights.x * mul(v.normal, (float3x3)boneTransform[v.boneIndiecs[0]]);
    normalL += v.weights.y * mul(v.normal, (float3x3)boneTransform[v.boneIndiecs[1]]);
    normalL += v.weights.z * mul(v.normal, (float3x3)boneTransform[v.boneIndiecs[2]]);
    normalL += v.weights.w * mul(v.normal, (float3x3)boneTransform[v.boneIndiecs[3]]);

    o.posW = mul(float4(v.pos, 1.0f), world);
    o.normalW = mul(normalL,(float3x3)worldInvTranspose);
    o.uv = v.uv;
    return o;
}

//mat1 = gTex.SampleLevel(g_samPoint, float2(baseU/1024, baseV/1024),0);
// 
//VertexPosHWNormalTex VShader(SkinnedVertexIn v)
//{
//    matrix Trasnform = boneTransform[v.boneIndiecs[0]] * v.weights.x;
//    Trasnform += boneTransform[v.boneIndiecs[1]] * v.weights.y;
//    Trasnform += boneTransform[v.boneIndiecs[2]] * v.weights.z;
//    Trasnform += boneTransform[v.boneIndiecs[3]] * v.weights.w;
//
//    VertexPosHWNormalTex o;
//    o.pos = mul(float4(v.pos, 1.0f), Trasnform);
//    //o.pos = mul(float4(v.pos, 1.0f), world);
//
//    //float4x4 instanceTransform = float4x4(g_instance[v.instanceId].world1,
//    //    g_instance[v.instanceId].world2,
//    //    g_instance[v.instanceId].world3,
//    //    0.0f, 0.0f, 0.0f, 1.0f);
//
//    float4x4 instanceTransform = float4x4(1.0f, 0.0f, 0.0f, 0.0f,
//        0.0f, 1.0f, 0.0f, 0.0f,
//        0.0f, 0.0f, 1.0f, 0.0f,
//        g_instance[v.instanceId].world1.w, 0.0f, g_instance[v.instanceId].world3.w, 1.0f);
//
//    o.pos = mul(o.pos, instanceTransform);
//    o.pos = mul(o.pos, world);
//    o.pos = mul(o.pos, view);
//    o.pos = mul(o.pos, proj);
//
//
//    //// o.normalW = mul(v.normal, (float3x3)worldInvTranspose);
//
//    float3 normalL = float3(0.0f, 0.0f, 0.0f);
//    normalL += v.weights.x * mul(v.normal, (float3x3)boneTransform[v.boneIndiecs[0]]);
//    normalL += v.weights.y * mul(v.normal, (float3x3)boneTransform[v.boneIndiecs[1]]);
//    normalL += v.weights.z * mul(v.normal, (float3x3)boneTransform[v.boneIndiecs[2]]);
//    normalL += v.weights.w * mul(v.normal, (float3x3)boneTransform[v.boneIndiecs[3]]);
//
//    o.posW = mul(float4(v.pos, 1.0f), world);
//    o.normalW = mul(normalL, (float3x3)worldInvTranspose);
//    o.uv = v.uv;
//    return o;
//}